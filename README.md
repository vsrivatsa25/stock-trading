
# Intelligent Stock Trading

The task of making an accurate prediction is an intricate as well as a highly difficult task. At the
same time, the highly rewarding nature of stock prediction makes it an ideal challenge for AI
Models. Our objective is to provide an AI-assisted platform with a highly accurate stock predicting
system that combines multiple prediction models.

## Run Locally

Clone the project

```bash
  git clone git clone https://Pratyparty@bitbucket.org/vsrivatsa25/stock-trading.git
```

Go to the project directory

```bash
  cd App/stockTrading
```

Install dependencies

```bash
  pip install -r requirements.txt
```

Start the server

```bash
  python3 manage.py runserver
```

  
## Authors

- [@vsrivatsa25](https://bitbucket.org/vsrivatsa25)
- [@Pratyparty](https://bitbucket.org/Pratyparty)

  
## Documentation

[Stock Trading](https://docs.google.com/document/d/1BK7JcH44iKlwmpXuEQqK6dK0N2CTLt_CxuxHkiwBfGA/edit?usp=sharing)

  
## Tech Stack

**Client:** HTML, CSS, JavaScript, Bootstrap

**Server:** Django

**Database:** MySQLite3

## Screenshots: 

**HOME PAGE:**

![image](https://user-images.githubusercontent.com/70327869/126195021-de921175-a06e-408d-8de5-19de714ecc4c.png)

**TCS STOCK PREDICTION:**

![image](https://user-images.githubusercontent.com/70327869/126195053-bb6349df-1070-4b88-8282-de887d1b51eb.png)


**INFY STOCK PREDICTION:**

![image](https://user-images.githubusercontent.com/70327869/126195071-966e7f35-a2cc-4c69-a5d7-72a529685998.png)


**DEPOSIT PAGE:**

![image](https://user-images.githubusercontent.com/70327869/126195085-4e7af0fc-fe38-4113-b748-c67583c2ca10.png)

**TRADES  PAGE:**

![image](https://user-images.githubusercontent.com/70327869/126195098-5e0dda1f-1452-4c1e-bd99-7dbcdb597489.png)

**PORTFOLIO  PAGE:**

![image](https://user-images.githubusercontent.com/70327869/126195119-b1f63c27-1d12-4dad-b9d6-9ed8a1842438.png)